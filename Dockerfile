# build image
FROM node:20 AS builder

RUN mkdir -p /home/node/app/node_modules && chown -R node:node /home/node/app

WORKDIR /home/node/app

RUN npm i -g npm@10.2.5 @angular/cli
# bitte beachtet das .dockerignore file. Hier stehen alle Verzeichnisse und Dateien drin, welche nicht kopiert werden sollen
COPY . .

RUN npm ci

RUN npx kendo-ui-license activate

RUN ng build

# final image
# Use the latest version of the official Nginx image as the base image§
FROM nginx:1.25
LABEL authors="KT AG, DFR, IT AG, IT-Bildung"
# copy the custom nginx configuration file to the container in the default
# location
COPY nginx.conf /etc/nginx/conf.d/default.conf
# copy the built application from the build stage to the nginx html
# directory
COPY --from=builder /home/node/app/dist/feedback-ui/browser/ /usr/share/nginx/html

EXPOSE 80
