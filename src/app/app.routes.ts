import { Routes } from '@angular/router';
import {ChessboardLayoutComponent} from "./components/chessboard-layout/chessboard-layout.component";

export const routes: Routes = [
  { path: 'chess', component: ChessboardLayoutComponent },
  // last
  { path: '**', redirectTo: 'chess' }
];

