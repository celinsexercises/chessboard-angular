import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ChessboardLayoutComponent } from './chessboard-layout.component';

describe('ChessboardLayoutComponent', () => {
  let component: ChessboardLayoutComponent;
  let fixture: ComponentFixture<ChessboardLayoutComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [ChessboardLayoutComponent]
    })
    .compileComponents();
    
    fixture = TestBed.createComponent(ChessboardLayoutComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
