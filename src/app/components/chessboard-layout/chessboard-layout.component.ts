import {Component, inject} from '@angular/core';
import {FormsModule} from '@angular/forms';
import {MatInputModule} from '@angular/material/input';
import {MatSelectModule} from '@angular/material/select';
import {MatFormFieldModule} from '@angular/material/form-field';
import {ChessboardContainerComponent} from "./chessboard-container/chessboard-container.component";
import {ChessboardFieldComponent} from "./chessboard-container/chessboard/chessboard-field/chessboard-field.component";
import {MatIconModule} from '@angular/material/icon';
import {MatDividerModule} from '@angular/material/divider';
import {MatButtonModule} from '@angular/material/button';
import {ChessboardService} from "../../services/chessboard.service";
import {ChessboardColorComponent} from "./chessboard-container/chessboard-color/chessboard-color.component";
import {MatProgressBar} from "@angular/material/progress-bar";
import {LevelComponent} from "./level/level.component";

/**
 * Size Select
 */
interface Field {
  value: string;
  viewValue: string;
}

@Component({
  selector: 'app-chessboard-layout',
  standalone: true,
  imports: [MatFormFieldModule, MatSelectModule, MatInputModule, FormsModule, ChessboardContainerComponent, ChessboardFieldComponent, MatButtonModule, MatDividerModule, MatIconModule, ChessboardColorComponent, MatProgressBar, LevelComponent],
  templateUrl: './chessboard-layout.component.html',
  styleUrl: './chessboard-layout.component.css'
})


export class ChessboardLayoutComponent {

  protected svc = inject(ChessboardService);

}

