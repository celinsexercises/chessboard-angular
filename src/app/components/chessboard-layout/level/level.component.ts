import {Component, inject} from '@angular/core';
import {ChessboardService} from "../../../services/chessboard.service";
import {ChessboardFieldComponent} from "../chessboard-container/chessboard/chessboard-field/chessboard-field.component";

@Component({
  selector: 'app-level',
  standalone: true,
  imports: [
    ChessboardFieldComponent
  ],
  templateUrl: './level.component.html',
  styleUrl: './level.component.css'
})
export class LevelComponent {
  protected svc = inject(ChessboardService);
}
