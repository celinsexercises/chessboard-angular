import {Component, inject} from '@angular/core';
import {ChessboardComponent} from "./chessboard/chessboard.component";
import {ChessboardService} from "../../../services/chessboard.service";
import {MatProgressBar} from "@angular/material/progress-bar";

@Component({
  selector: 'app-chessboard-container',
  standalone: true,
  imports: [
    ChessboardComponent,
    MatProgressBar,
  ],
  templateUrl: './chessboard-container.component.html',
  styleUrl: './chessboard-container.component.css'
})
export class ChessboardContainerComponent {
  protected svc = inject(ChessboardService);

}
