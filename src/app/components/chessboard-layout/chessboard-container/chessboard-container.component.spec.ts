import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ChessboardContainerComponent } from './chessboard-container.component';

describe('ChessboardContainerComponent', () => {
  let component: ChessboardContainerComponent;
  let fixture: ComponentFixture<ChessboardContainerComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [ChessboardContainerComponent]
    })
    .compileComponents();
    
    fixture = TestBed.createComponent(ChessboardContainerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
