import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ChessboardColorComponent } from './chessboard-color.component';

describe('ChessboardColorComponent', () => {
  let component: ChessboardColorComponent;
  let fixture: ComponentFixture<ChessboardColorComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [ChessboardColorComponent]
    })
    .compileComponents();
    
    fixture = TestBed.createComponent(ChessboardColorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
