import {Component, inject} from '@angular/core';
import {ChessboardService} from "../../../../services/chessboard.service";

@Component({
  selector: 'app-chessboard-color',
  standalone: true,
  imports: [],
  templateUrl: './chessboard-color.component.html',
  styleUrl: './chessboard-color.component.css'
})
export class ChessboardColorComponent {
  protected svc = inject(ChessboardService);

}
