import {Component, inject, Input, OnInit, signal} from '@angular/core';
import {MatOption} from "@angular/material/autocomplete";
import {ChessboardService} from "../../../../../services/chessboard.service";
import {ChessboardColorComponent} from "../../chessboard-color/chessboard-color.component";
import {BoxType, SelectableColor} from "../../../models";

@Component({
  selector: 'app-chessboard-field',
  standalone: true,
    imports: [
        ChessboardColorComponent
    ],
  templateUrl: './chessboard-field.component.html',
  styleUrl: './chessboard-field.component.css'
})
export class ChessboardFieldComponent implements OnInit {
  protected svc = inject(ChessboardService);

  @Input({required: true})
  type!: BoxType;

  @Input({required: true})
  id!: number;

  backgroundColor = signal<SelectableColor>('white');
  ngOnInit() {
    this.setBackgroundColor();
  }

  onClick() {
    // bei einem Klick auf das Template soll nicht passieren, aif das Game jedoch schon
    if (this.type === 'template') {
      return;
    }
    this.svc.setGameColor(this.id);
    this.setBackgroundColor();
  }


  private setBackgroundColor() {
    if (this.type === 'template') {
      this.backgroundColor.set(this.svc.templateArray[this.id]);
    } else if (this.type === 'game') {
      this.backgroundColor.set(this.svc.gameArray[this.id]);
    }
  }
}
