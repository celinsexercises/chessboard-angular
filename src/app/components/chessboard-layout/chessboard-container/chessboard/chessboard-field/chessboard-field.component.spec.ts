import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ChessboardFieldComponent } from './chessboard-field.component';

describe('ChessboardFieldComponent', () => {
  let component: ChessboardFieldComponent;
  let fixture: ComponentFixture<ChessboardFieldComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [ChessboardFieldComponent]
    })
    .compileComponents();
    
    fixture = TestBed.createComponent(ChessboardFieldComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
