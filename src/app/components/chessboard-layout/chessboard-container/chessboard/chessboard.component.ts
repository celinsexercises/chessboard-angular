import {Component, effect, inject, Input, OnInit, signal} from '@angular/core';
import {ChessboardFieldComponent} from "./chessboard-field/chessboard-field.component";
import {ChessboardService} from "../../../../services/chessboard.service";
import {BoxType} from "../../models";


@Component({
  selector: 'app-chessboard',
  standalone: true,
  imports: [
    ChessboardFieldComponent,
  ],
  templateUrl: './chessboard.component.html',
  styleUrl: './chessboard.component.css'
})
export class ChessboardComponent {
  protected svc = inject(ChessboardService);

  @Input({required: true})
  type!: BoxType


}
