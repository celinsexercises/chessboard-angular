import {Injectable, signal} from '@angular/core';

type selectableColor = 'white' | 'red'| 'blue'| 'pink'| 'green'| 'purple'| 'orange';

@Injectable({
  providedIn: 'root'
})
export class ChessboardService {

  boxSize = signal<number>(30);
  possibleColors = signal<selectableColor[]>(['red', 'blue', 'pink', 'green', 'purple', 'orange']);
  selectedColor = signal<selectableColor>('white');
  fields = signal<number[]>([2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16]);
  selectedFields = signal<number>(0)

  duration = signal<string>(''); // wir initialisieren mit einem leeren String
  isRunning = signal<boolean>(false);

  clickCount = signal<number>(0);
  selectedLevel = signal<("easy" | "medium" | "advanced")[]>([]);

  highestCompletedField= signal(0);

  // Stoppuhr
  private startTimestamp = 0;
  templateArray: selectableColor[] = [];
  gameArray: selectableColor[] = [];
  //Verfolgt die abgeschlossenen Level des Spielers
  private completedLevels: boolean[] = [false, false, false];


  constructor() {
    // Implementiert einen Loop, welcher die Duration rechnet
    setInterval(() => {
      if (!this.isRunning()) {
        return;
      }
      // Berechnet die Duration
      const endTimestamp = +(new Date());
      const differenzInMS = Math.abs(endTimestamp - this.startTimestamp);

      // Berechnet die Zeitangaben
      const sekunden = Math.floor(differenzInMS / 1000);
      const minuten = Math.floor(sekunden / 60);
      const stunden = Math.floor(minuten / 60);

      // Formatiert die Zeitdauer
      const formatierteZeitdauer = `${stunden} Stunden, ${minuten % 60} Minuten und ${sekunden % 60} Sekunden`;

      this.duration.set(formatierteZeitdauer);
    }, 900);

  }


  

  //Levels
  setLevel(level: 'easy' | 'medium' | 'advanced') {
    const levelsMap = {
      'easy': [2, 3, 4, 5],
      'medium': [6, 7, 8, 9, 10],
      'advanced': [11, 12, 13, 14, 15, 16]
    };
    this.fields.set(levelsMap[level]);
    //Setzt das ausgewählt Feld auf das erste Feld des Levels
    this.selectedFields.set(levelsMap[level][0]);

    switch (level) {
      case "easy":
        this.possibleColors.set(['red', 'blue']);
        break;
      case 'medium':
        this.possibleColors.set(['red', 'blue', 'pink', 'green']);
        break;
      case 'advanced':
        this.possibleColors.set(['red', 'blue', 'pink', 'green', 'purple', 'orange']);
        break;
    }
    this.selectedLevel.set([level]);
  }

  isLevelAvailable(level: 'easy' | 'medium' | 'advanced'): boolean {
    switch (level) {
      case 'easy':
        return !this.completedLevels.some((completed, index) => index > 0 && !completed);
      case 'medium':
        return !this.completedLevels[1];
      case 'advanced':
        return !this.completedLevels[2];
      default:
        return false;
    }
  }

  //Initalisiert completedFields Array mit false für jedes Feld


  // Aktive Farbe setzen
  setActiveColor(color: selectableColor) {
    this.selectedColor.set(color);
  }

  // Spiel starten
  startGame() {
    // Klick-zähler zurücksetzen
    this.clickCount.set(0);
    // Startzeit setzen
    this.startTimestamp = +(new Date());
    // Game beginnt!
    this.isRunning.set(true);
  }

  // Berechnung der Vergangenen Zeit nach Start des Spiels
  getDuration(endTimestamp: number) {
    // Differenz zwischen End- und Startzeitpunkt berechnen
    const timeDifference = endTimestamp - this.startTimestamp;

    // Umrechnung von Millisekunden in Sekunden und Rundung auf ganze Sekunden
    return Math.floor(timeDifference / 1000);
  }

  // Hier kommt die Methode, welche erlaubt im Game eine Farbe zu setzen
  setGameColor(index: number) {
    // Mögliche Varianten. Wenn es bereits die gleiche Farbe ist, die Farbe auf weiss setzen
    if (this.gameArray[index] === this.selectedColor()) {
      this.gameArray[index] = 'white';
    } else {
      this.gameArray[index] = this.selectedColor();
    }
    // Klick-zähler
    const currentClickCount = this.clickCount();
    this.clickCount.set(currentClickCount + 1);
  }


  // Generiert Schachbrett-Template
  private generateChessTemplate() {
    //Erstellt ein Array mit der gewünschten Länge
    this.gameArray = new Array(this.selectedFields() * this.selectedFields());
    this.templateArray = new Array(this.selectedFields() * this.selectedFields());
    // Erstellt ein Template mit zufälligen Farben
    for (let i = 0; i < (this.selectedFields() * this.selectedFields()); i++) {
      const randomIndex = Math.floor(Math.random() * this.possibleColors().length);
      // Eine Farbe aus den möglichen Farben auswählen und reinschreiben
      this.templateArray[i] = this.possibleColors()[randomIndex];
    }
  }


  validateGame() {
    for (let i = 0; i < this.templateArray.length; i++) {
      if (this.gameArray[i] !== this.templateArray[i]) {
        return alert('Das ist falsch!');
      }
    }
    // Endzeitpunkt
    const endTimestamp = +(new Date());

    // Spielzeit berechnen
    const duration = this.getDuration(endTimestamp);

    // Spielzeit formatieren
    const formattedDuration = this.formatDuration(duration);

    alert('Das ist richtig! Du hast ' + formattedDuration + ' benötigt und ' + this.clickCount() + ' Klicks.');
    this.highestCompletedField.set(this.selectedFields());
    this.resetGame();
  }

  resetGame() {
    this.gameArray = [];
    this.selectedFields.set(this.selectedFields() + 1); // Zurücksetzen der ausgewählten Felder auf 0
    this.isRunning.set(false);
  }
  // Zeitformat neu berechnen
  formatDuration(duration: number): string {
    const hours = Math.floor(duration / 3600);
    const minutes = Math.floor((duration % 3600) / 60);
    const seconds = duration % 60;

    return `${hours} Stunden, ${minutes} Minuten und ${seconds} Sekunden`;
  }


  // Nachricht für Fertigstellung eines Levels
  gameCompleted() {
    alert('Herzlichen Glückwunsch! Du hast Level ' + this.selectedLevel() + ' erfolgreich abgeschlossen.');
  }




}



