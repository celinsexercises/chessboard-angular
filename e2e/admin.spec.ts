import { test } from '@playwright/test';

// Test Description
test.describe('Chessboard Game', () => {
  test('Start Button Test', async ({page}) => {
    // URL aufrufen
    await page.goto('');
    // Klickt Start Button
    await page.click('button');


    // Klickt Check Button
    await page.getByRole('button', {name: 'Check'}).click();
  })
})
